#include"pentagono.hpp"

Pentagono::Pentagono(){

};
Pentagono::Pentagono(string tipo, float base, float altura){
   set_tipo(tipo);
   set_base(base);
   set_altura(altura);
};

Pentagono::~Pentagono(){

};

float Pentagono::calcula_area(){
   float apotema  = get_altura()/2;
   return calcula_perimetro()*apotema/2;
};

float Pentagono::calcula_perimetro(){
   return 5*get_base();
};