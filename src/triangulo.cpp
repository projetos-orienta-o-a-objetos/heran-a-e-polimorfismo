#include "triangulo.hpp"
#include<iostream>

Triangulo::Triangulo(){

}
Triangulo::Triangulo(string tipo, float base, float altura){
   set_tipo(tipo);
   set_base(base);
   set_altura(altura);
}
Triangulo::~Triangulo(){

}

float Triangulo::calcula_area(){
   return (get_base()*get_altura())/2;
}

float Triangulo::calcula_perimetro(){
   return get_base()+2*get_altura();
}

void Triangulo::imprime_dados(){
   cout<<"Tipo: "<< get_tipo()<< endl;
   cout<<"Base: "<< get_base()<<endl;
   cout<<"Altura:"<< get_altura()<<endl;
   cout<<"Area: "<< calcula_area()<<endl;
   cout<<"Perimetro: "<< calcula_perimetro()<<endl;
}