#include "hexagono.hpp"
#include <math.h>
#include<iostream>

Hexagono::Hexagono(){

}
Hexagono::Hexagono(string tipo, float base){
   set_tipo(tipo);
   set_base(base);
   set_altura(10);
}
Hexagono::~Hexagono(){

}

float Hexagono:: calcula_area(){
 return  (3*sqrt(3)*(pow (get_base(), 2)))/2;
}

float Hexagono:: calcula_perimetro(){
   return 6*get_base();
}
 void Hexagono::imprime_dados(){
   cout<<"Tipo: "<< get_tipo()<< endl;
   cout<<"Base: "<< get_base()<<endl;
   cout<<"Area: "<< calcula_area()<<endl;
   cout<<"Perimetro: "<< calcula_perimetro()<<endl;
 }