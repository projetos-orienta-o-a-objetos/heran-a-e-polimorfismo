#include<iostream>
#include<vector>
#include "formageometrica.hpp"
#include"quadrado.hpp"
#include"triangulo.hpp"
#include"hexagono.hpp"
#include"paralelogramo.hpp"
#include"pentagono.hpp"
#include "circulo.hpp"

int main(){
   vector<FormaGeometrica*> formas;
   Quadrado quadradado1 = {"Quadrado",2};
   Triangulo triangulo1 = {"Triangulo", 4, 2};
   Hexagono hexagono = {"Hexagono", 5};
   Paralelogramo paralelogramo = {"Paralelogramo", 10, 3};
   Pentagono pentagono = {"Pentagono", 6, 10};
   Circulo circulo = {"Circulo", 4};
   
   formas.push_back(&quadradado1);
   formas.push_back(&triangulo1);
   formas.push_back(&hexagono);
   formas.push_back(&paralelogramo);
   formas.push_back(&pentagono);
   formas.push_back(&circulo);


   for(auto &forma : formas){
      forma->imprime_dados();
   }
   
   return 0;
}