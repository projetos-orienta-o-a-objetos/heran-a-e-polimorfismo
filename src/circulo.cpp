#include "circulo.hpp"
#include<iostream>
Circulo::Circulo(){

}
Circulo::Circulo( string tipo, float raio){
   set_tipo(tipo);
   set_altura(2*raio);
   set_base(2*raio);
   set_raio(raio);
}
Circulo::~ Circulo(){

}
float Circulo:: get_raio(){
   return raio;
}
void Circulo:: set_raio(float raio){
   if (raio<0)
      return;
   else
      this->raio = raio;
}

float Circulo::calcula_area(){
   return 3.14*raio*raio;
}

float Circulo::calcula_perimetro(){
   return 2*3.14*get_raio();
}

void Circulo::imprime_dados(){
   cout<<"Tipo: "<< get_tipo()<< endl;
   cout<<"Raio:"<< get_raio()<<endl;
   cout<<"Area: "<< calcula_area()<<endl;
   cout<<"Perimetro: "<< calcula_perimetro()<<endl;
}