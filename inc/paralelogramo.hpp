#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP
#include "formageometrica.hpp"

using namespace std;

class Paralelogramo : public FormaGeometrica
{
private:
  
public:
   Paralelogramo(string tipo, float base, float altura);
   Paralelogramo();
   ~Paralelogramo(); 
};

#endif