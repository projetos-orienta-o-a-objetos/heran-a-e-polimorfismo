#ifndef CIRCULO_HPP
#define CIRCULO_HPP
#include "formageometrica.hpp"

using namespace std;

class  Circulo : public FormaGeometrica{
private:
   float raio;
   //A base será o perímetro do raio
public:
   Circulo();
   Circulo(string tipo, float raio);
   ~ Circulo();
   float get_raio();
   void set_raio(float raio);
   float calcula_area();
   float calcula_perimetro();
   void imprime_dados();
};

#endif