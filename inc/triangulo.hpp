#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP
#include "formageometrica.hpp"

using namespace std;

class Triangulo: public FormaGeometrica{
private:
   
public:
   Triangulo();
   Triangulo(string tipo, float base, float altura);
   ~Triangulo();
   float calcula_area();
   float calcula_perimetro();
   void imprime_dados();
};

#endif