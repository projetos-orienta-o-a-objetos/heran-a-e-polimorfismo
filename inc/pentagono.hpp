#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP
#include "formageometrica.hpp"

using namespace std;
class Pentagono: public FormaGeometrica
{
private:

public:
   Pentagono();
   Pentagono(string tipo, float base, float altura);
   ~Pentagono();
   float calcula_area();
   float calcula_perimetro();
};


#endif