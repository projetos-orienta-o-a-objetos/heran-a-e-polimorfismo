#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP
#include"formageometrica.hpp"

using namespace std;

class Hexagono: public FormaGeometrica{
private:
  
public:
   Hexagono();
   Hexagono(string tipo, float base);
   ~Hexagono();
   float calcula_area();
   float calcula_perimetro();
   void imprime_dados();
};

#endif