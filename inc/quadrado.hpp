#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"


using namespace std;

class Quadrado : public FormaGeometrica{
private:
   
public:
   Quadrado();
   Quadrado(string tipo, float base);
   ~Quadrado();
};

#endif